// SearchEngine.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
#include <time.h>


using namespace std;
// implementing a hash table and a binary tree search function.
// using an external file for the terms


//class for elements in the lists
class myDbElement{
public:
	string term = "", def = "";
};
//binary tree node class.
class node : public myDbElement{
public:
	
	node(){
		left = NULL;
		right = NULL;
		prev = NULL;
	}
	node(string t, string d){ //term and def
		if (t.substr(0, 1) == "\n"){
			t = t.substr(1);
		}
		term = t;
		def = d;
		left = NULL;
		right = NULL;
		prev = NULL;
		//cout << "New node created with term: " << t << endl;
	}
	node* getLeft(void) const{
		return left;
	}
	node* getRight(void) const{
		return right;
	}
	node* getPrev(void) const{
		return prev;
	}
	string getDef(void) const{
		return def;
	}
	string getTerm(void) const{
		return term;
	}
	void setLeft(node* l){
		left = l;
	}
	void setRight(node* r){
		right = r;
	}
	void setPrev(node* p){
		prev = p;
	}
	void setTerm(string t){
		term = t;
	}
	void setDef(string d){
		def = d;
	}


private:
	node *left, *right, *prev;
};
//binary tree class
class btree{
private:
	node* parent;
	bool found = false;
	void destroyMe(node* n){
		if (n != NULL){
			destroyMe(n->getLeft());
			destroyMe(n->getRight());
			delete n;
		}
	}

	node* searchNode(string t, node* n){
		counter++;
		if (n != NULL){
			transform(t.begin(), t.end(), t.begin(), ::tolower);
			string lowTerm = n->getTerm();
			transform(lowTerm.begin(), lowTerm.end(), lowTerm.begin(), ::tolower);
			if (lowTerm.substr(0, 1) == "\n"){
				lowTerm = lowTerm.substr(1);
			}
			string subTerm = lowTerm.substr(0, (t.length()));
			if (subTerm.compare(t) == 0) {
				cout << "\n\nMatch: " << endl;
				cout << n->getTerm() << ": " << n->getDef() << endl;
				cout << "Found in cycle: " << counter << endl;
				found = true;
				return n;
			}
			if ((lowTerm.find(t) != std::string::npos)){
				cout << "\n\n Partial match: " << endl;
				cout << n->getTerm() << ": " << n->getDef() << endl;
				cout << "Found in cycle: " << counter << endl;
				found = true;
			}
			//debug part
			/*
			cout << "\n\nTerm: " << lowTerm << endl;
			cout << "Searching for: " << t << endl;
			cout << "Compared value: "<< lowTerm.compare(t) << endl;
			cout << "Sub compared value: " << subTerm.compare(t) << endl << endl;
			*/
			if (subTerm.compare(t) < 0){
				
				searchNode(t, n->getRight());
			}
			else{
				searchNode(t, n->getLeft());
				
			}
			
			
		}
		else{
			return NULL;
		}
	}
	
	void addNode(string t, string d, node* n){
		string term = n->getTerm();
		
		if (term.substr(0, 1) == "\n"){
			term = term.substr(1);
		}
		transform(term.begin(), term.end(), term.begin(), ::tolower);

		if (t.substr(0, 1) == "\n"){
			t = t.substr(1);
		}
		string tLower = t;
		transform(tLower.begin(), tLower.end(), tLower.begin(), ::tolower);
		if ((term.compare(tLower)) < 0){
			if (n->getRight() != NULL){
				addNode(t, d, n->getRight());
			}
			else{
				node* current = new node(t, d);
				n->setRight(current);
				current->setPrev(n);
			}
		}
		else if ((term.compare(tLower)) > 0){
			if (n->getLeft() != NULL){
				addNode(t, d, n->getLeft());
			}
			else{
				node* current = new node(t, d);
				n->setLeft(current);
				current->setPrev(n);
			}
		}
		else{
			cout << "Term is already in the binary tree. Not added."<< endl;
		}
	}


public:
	int counter;
	btree(){
		parent = NULL;
		counter = 0;

	}
	~btree(){
		destroyMe(parent);
	}
	void search(string t){
		counter = 0;
		found = false;
		searchNode(t, parent);
		if (!found){
			cout << "\n\nNo match found.\n" << endl;
		}
	}
	void add(string t, string d){
		if (t.length() > d.length()){
			cout << "Possible error in data file." << endl;
			cout << "Term: " << t << endl;
			cout << "Definition: " << d << endl;
		}
		if (t.length() < 2){
			cout << "Possible error in data file." << endl;
			cout << "Term: " << t << endl;
		}
		if (d.length() < 10){
			cout << "Possible error in data file." << endl;
			cout << "Definition: " << d << endl;
		}
		if (parent == NULL){
			parent = new node(t, d);
		}
		else{
			addNode(t, d, parent);
		}
	}
};

//data handler class
class dataHandler{

public:

	string line;
	ifstream myfile;
	myDbElement *list;
	string termFileName;
	int listSize = 0;
	btree binTree;

	dataHandler(){}

	dataHandler(string fileName){
		myfile.open(fileName);
		if (myfile.is_open()){
			cout << "Terms are loaded from file " << fileName << "." << endl;
			termFileName = fileName;
		}
		else{
			cout << "Terms file not found: " << fileName << endl;
		}
		myfile.close();
	}

	int countLinesInFile(){
		myfile.open(termFileName);
		int termsCounter = 0;
		if (myfile.is_open())
		{
			while (getline(myfile, line, ':'))
			{
				//cout << line << '\n';
				termsCounter++;
			}
			myfile.close();
			return termsCounter;
		}
		else cout << "Unable to open file.";
	}


	unsigned int doHash(string input)
	{
		//cout << input << endl;
		transform(input.begin(), input.end(), input.begin(), ::tolower);
		if (input.substr(0, 1) == "\n") {
			input = input.substr(1);
		}
		int hash = 0;
		char c = ' ';
		int inputDecimal = 0;
		for (int i = 0; i < input.length(); i++){
			c = input.at(i);
			//cout << c << endl;
			inputDecimal = c;
			//cout << inputDecimal << endl;
			hash += inputDecimal;
			//cout << hash << endl;
			if (i == 3) return (hash * 7) / 3; //calculate the 1st 4 char from the input string
		}
		//if the input is less than 4 char long then return the current hash !! and add space ? experimenting !!
		//hash *= 6;
		return (hash * 7) / 3;
	}

	int getFreePos(int pos){

		do{
			if (list[pos].term == ""){
				//cout << "Position allocated at: "<< pos << endl;
				return pos;
			}
			else{
				pos++;
			}
		} while (pos <= listSize-1);
		return -1;
	}



	void createHashTable(unsigned long sizeMultiplier){
		listSize = countLinesInFile();
		//if (listSize < 1200) listSize = 1200;
		if (sizeMultiplier > 1){
			listSize = listSize * sizeMultiplier;
		}
		cout << "List size: " << listSize << endl;
		list = new (nothrow)myDbElement[listSize];
		if (list == nullptr){
			cout << "Error: memory could not be allocated.";
			return;
		}
		int toggle = 0;
		int hashPos = 0;
		string term = "";
		myfile.open(termFileName);
		if (myfile.is_open())
		{
			while (getline(myfile, line, ':'))
			{
				//cout << "Line: "<< line << endl;
				if (toggle > 0){
					toggle = 0;
					if (term.length() > line.length()){
						cout << "Possible error in terms file." << endl;
						cout << "Term: " << term << endl;
						cout << "Definition" << line << endl;
					}
					if (term.length() < 2){
						cout << "Possible error in terms file." << endl;
						cout << "Term: " << term << endl;
					}
					if (line.length() < 10){
						cout << "Possible error in terms file." << endl;
						cout << "Definition" << line << endl;
					}
					list[hashPos].def = line;
					list[hashPos].term = term;
				}
				else{
					int pos = doHash(line);
					hashPos = getFreePos(pos);
					//cout << hashPos << endl;
					term = line;
					//cout << list[hashPos].term << endl;
					toggle=1;
				}
			}
			myfile.close();

		}
		else cout << "Unable to open file.";
	}

	void printHashTable(){
		int i = 0;
		for (i = 0; i < listSize - 1; i++){
			if (list[i].term != ""){
				cout << "Position: " << i << " " << list[i].term << ": " << list[i].def << endl;
			}
			else{
				cout << "Position: " << i << " Empty space." << endl;
			}
			if (i % 12 == 0){
				cin.get();
			}
		}
		cout << i << endl;
	}

	void searchInHashTable(string s){
		int searchPos = 0;
		transform(s.begin(), s.end(), s.begin(), ::tolower);
		if (s.length() < 2){
			cout << "Search term is too short. Please enter 2 or more characters." << endl;
			return;
		}

		searchPos = doHash(s);
		cout << "\nSearching for: " << s << " hashPos: " << searchPos << endl;
		int found = 0;
		int counter = 0;
		string term = "";
		do{
			term = list[searchPos].term;
			transform(term.begin(), term.end(), term.begin(), ::tolower);
			int result = term.find(s);
			if (result != std::string::npos){
				cout << list[searchPos].term << ": " << list[searchPos].def << endl;
				cout << "Match found in cycle: " << counter << endl;
				found++;
				searchPos++;
			}
			else{
				searchPos++;
			}
			counter++;
		} while (term != "");
		if (found == 0){
			cout << "\n\nNo match found.\n" << endl;
		}
		cout << "Search cycles: " << counter << endl;
	}

	void searchInFullHash(string s){
		int searchPos = 0;
		cout << "\n\nFull hash table scan for debug (and proof)" << endl;
		transform(s.begin(), s.end(), s.begin(), ::tolower);
		if (s.length() < 2){
			cout << "Search term is too short. Please enter 2 or more characters." << endl;
			return;
		}

		//searchPos = doHash(s);
		cout << "\nSearching for: " << s << " hashPos should be: " << doHash(s) << endl;
		int found = 0;
		string term = "";
		do{
			term = list[searchPos].term;
			transform(term.begin(), term.end(), term.begin(), ::tolower);
			int result = term.find(s);
			if (result != std::string::npos){
				cout << "Pos: " << searchPos << " Term:" << list[searchPos].term << endl;
				found++;
				searchPos++;
			}
			else{
				searchPos++;
			}
		} while (searchPos < (listSize));
		if (found == 0){
			cout << "\n\nNo match found.\n" << endl;
		}

	}


	void createBinaryTree(){
		myfile.open(termFileName);
		int toggle = 0;
		string t, d, line;	//(t)erm, (d)ef
		if (myfile.is_open())
		{
			while (getline(myfile, line, ':'))
			{
				
				if (toggle == 0){
					t = line;
					toggle++;
				}
				else
				{
					d = line;
					binTree.add(t, d);
					toggle = 0;
				}
			}

		}
		myfile.close();
	}
};




//the main class
class mainApp{
	int myInput = 0;
	string searchFor = "";
	bool binaryTreeUp = false;
	
public :
	mainApp(){
		cout << "Welcome!" << endl;
		cout << "Usage:\nSelect your search algorithm\nEnter your search term.\n";
	}

	void printPart2(){
		
		cout << "Enter your search term:";
	}

	void printPart1(){
		cout << "\nSearch algorithms:\n1.Binary tree\n2.Hash table\n0.Exit\n";
		cout << "Select search algorithm:";
	}


	int readInputInt(){
		string input = "";
		int result = 0;
		while (true){
			//cout << "Please enter a number:" << endl;
			cin.clear();
			getline(cin, input);
			stringstream myStream(input);
			if (myStream >> result){
				break;
			}
			cout << "Invalid input." << endl;
		}
		return result;
	}

	string readInputString(){
		string input = "";
		cin.clear();
		getline(cin, input);
		return input;
	}
	
	void myMain(){
		dataHandler dh("terms.txt");
		clock_t t0, t1, t2;
		int elapsed = 0, elapsed2 = 0;
		do{
			printPart1();
			myInput = readInputInt();
			if (myInput == 0) break;
			printPart2();
			searchFor = readInputString();
			if (myInput == 1){				
				
				t0 = clock();
				if (binaryTreeUp != true){
					dh.createBinaryTree();
					binaryTreeUp = true;
				}
				cout << "Binary tree stuff here" << endl;
				dh.binTree.search(searchFor);
				cout << "\nSearch cycles: " << dh.binTree.counter << endl;
				t1 = clock();
				elapsed = t1 - t0;
				cout << "Function run for: " << elapsed << " clicks." << endl;
			}
			else if (myInput == 2){
				
				cout << "Hash table stuff here" << endl;
				dh.createHashTable(4);
				//dh.printHashTable();
				
				//cout << dh.doHash(searchFor) << endl;
				t0 = clock();
				dh.searchInHashTable(searchFor);
				t1 = clock();
				dh.searchInFullHash(searchFor); 

				t2 = clock();
				elapsed = t1 - t0;
				elapsed2 = t2 - t1;
				cout << "1st seearch function run for: " << elapsed << " clicks." << endl;
				cout << "Full hash search function run for: " << elapsed2 << " clicks." << endl;
			}
			else{
				cout << "Please select a valid option." << endl;
			}

		} while (myInput != 0);
		cout << "Goodbye!";
		cin.get();
	}
};



int _tmain(int argc, _TCHAR* argv[])
{

	mainApp myApp;
	myApp.myMain();
	return 0;
}

